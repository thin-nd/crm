<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    //
    public function viewSignup()
    {
        return view('layout.signup');
    }

    public function signup(Request $request)
    {
        $user = $request->all();
        $this->user->signup($user);

        return redirect()->route('login');
    }

    public function viewLogin()
    {
        return view('layout.login');
    }
    public function login(Request $request)
    {
        $checkExistyUser = User::where('username', $request->username )->first();
        if (!$checkExistyUser) {
            return redirect()->route('login')->with('error', 'tài khoản không tồn tại');
        }
        elseif(Hash::check($request->password, $checkExistyUser['password'])) {
            return redirect()->route('home')->with('key', 'Đăng nhập thành công');
        }else {
            return redirect()->route('login')->with('error', 'Sai mật khẩu');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
