<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    const ADMIN = 1;
    const USER = 2;

    public function handle($request, Closure $next, $role)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $method = $role;
        if (method_exists($this, $method)) {
            return $this->{$method}($request, $next);
        }
        return redirect()->route('login');
    }

    protected function admin($request, $next) {
        $user = Auth::user();
        if ($user->role !== self::ADMIN) {
            return redirect()->route('login');
        }
        return $next($request);
    }

    protected function teacher($request, $next) {
        $user = Auth::user();
        if ($user->user_type !== self::USER) {
            return redirect()->route('login');
        }
        return $next($request);
    }
}
