<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Admin extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'admins';

    protected $fillable = [
        'username', 'password', 'role'
    ];
    public function createAdmin()
    {
        Admin::create([
            'username' => 'Taylor',
            'password' => '10',
            'role' => 1
        ]);
    }
}
