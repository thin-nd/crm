<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Eloquent implements Authenticatable
{
    use Notifiable;

    protected $connection = 'mongodb';
    protected $collection = 'users';

    protected $fillable = [
        'full_name',
        'username',
        'email',
        'password',
        'age', 'gender', 'phone', 'address', 'job', 'role', 'company'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function signup($request)
    {
        $addUser = new User();

        $addUser->username = $request['username'];
        $addUser->password = Hash::make($request['password']);
        $addUser->full_name = $request['full_name'];
        $addUser->email = $request['email'];
        $addUser->role = 1;

        $addUser->save();
    }
    public function getAuthIdentifierName(){}
    public function getAuthIdentifier(){}
    public function getAuthPassword(){}
    public function getRememberToken(){}
    public function setRememberToken($value){}
    public function getRememberTokenName(){}
}
