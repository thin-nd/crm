<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//login register
Route::get('/register', [AuthController::class, 'viewSignup'])->name('viewregister');
Route::post('/register', [AuthController::class, 'signup'])->name('register');
Route::get('/login', [AuthController::class, 'viewLogin'])->name('viewlogin');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Route::get('/home', function () {
    return view('admin.createuser');
})->name('home');

Route::get('/admin',[AdminController::class, 'createAdmin']);

